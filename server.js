/**
 * Created by ppa on 01.12.2015.
 */
var restify = require('restify');
var server = restify.createServer();
var http = require('http');
var overpass = require('query-overpass');
var reproject = require('reproject-spherical-mercator');
var mercator = require('sphericalmercator');

server.use(restify.bodyParser());
server.use(restify.queryParser());

server.use(
    function crossOrigin(req,res,next){
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        return next();
    }
);

server.get('/map/buildings', function(req, res, next){
    var query = '[out:json][timeout:25];' +
    '(' +
    'way["highway"](' + req.params.bbox + ');' +
    'way["building"](' + req.params.bbox + ');' +
    'relation["building"](' + req.params.bbox + ');' +
    'node["leisure"="park"](' + req.params.bbox + ');' +
    'way["leisure"="park"](' + req.params.bbox + ');' +
    'relation["leisure"="park"](' + req.params.bbox + ');' +
    'way["landuse"="forest"](' + req.params.bbox + ');' +
    'relation["landuse"="forest"](' + req.params.bbox + ');' +
    ')' +
    ';out body;' +
    '>;' +
    'out skel qt;';

    //var query = '[out:json][timeout:25];(way["highway"](' + req.params.bbox + ');way["building"](' + req.params.bbox + ');relation["building"](' + req.params.bbox + '););out body;>;out skel qt;';

    overpass(query, function(err, data){
        res.send(reproject(data));
        next();
    });
});

server.get('/map/streets', function(req, res, next){

    var query = '[out:json][timeout:25];(way["highway"](' + req.params.bbox + '););out body;>;out skel qt;';

    overpass(query, function(err, data){
        console.log(data);
        res.send(data);
        next();
    });
});

var port = 8090;

server.listen(port, function (err) {
    if (err) {
        console.error(err);
    }
    else {
        console.log('App is ready at : ' + port);
    }
});