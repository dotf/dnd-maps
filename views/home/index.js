/**
 * Created by ppa on 01.12.2015.
 */
/* global angular, DOTF, d3 */
(function () {
    //'use strict';

    function HomeCtrl($http) {
        var self = this;

        this.getZoomLevel = function () {
            console.log(self.map.getZoom());
        };

        var sm = new SphericalMercator();

        this.drawBuildings = function () {
            self.isBusy = true;
            var paths = [];
            var bounds = self.map.getBounds();
            var northEast = bounds.getNorthEast();
            var southWest = bounds.getSouthWest();

            var rect = {
                left: southWest.lng,
                bottom: southWest.lat,
                right: northEast.lng,
                top: northEast.lat
            };

            var bbox = rect.bottom + ',' + rect.left + ',' + rect.top + ',' + rect.right;
            $http.get(DOTF.apis.dotf + '/map/buildings?bbox=' + bbox).success(function (data) {

                var container = document.getElementById('mapArea'),
                    width = container.offsetWidth,
                    svgMap = $('#svg');
                svgMap.empty();
                svgMap.attr('width', width);
                svgMap.attr('height', 600);

                var options = {
                    viewportSize: {width: width, height: 600},
                    mapExtent: {
                        left: sm.forward([rect.left, 0])[0],
                        bottom: sm.forward([0, rect.bottom])[1],
                        right: sm.forward([rect.right, 0])[0],
                        top: sm.forward([0, rect.top])[1]
                    }
                };

                console.log(options);

                var converter = geojson2svg(options);

                data.features.forEach(function(f){
                    var svgString, svg;
                    svgString = converter.convert(f);

                    if (f.properties.tags.height){
                        console.log(f.properties.tags.height);
                    }
                    svg = parseSVG(svgString);
                    paths.push(svg.children[0].getAttribute('d'));

                    if (f.properties.tags.highway) {
                        svg.firstElementChild.setAttribute('class', f.properties.tags.highway);
                    }else if (f.properties.tags.leisure && f.properties.tags.leisure === 'park'){
                        svg.firstElementChild.setAttribute('class', 'park');
                    }else if (f.properties.tags.landuse && f.properties.tags.landuse === 'forest'){
                        svg.firstElementChild.setAttribute('class', 'forest');
                    }
                    svgMap.append(svg);
                });

                self.hasDrawing = true;
                self.isBusy = false;

                draw3D(paths);
            });
        };


        function draw3D(paths){
            var renderer;
            var controls;
            var scene;
            var camera;
            var $container;
            var clock;

            init();
            animate();

            function init() {
                clock = new THREE.Clock();

                $container = $('#container');
                var width = window.innerWidth * 0.99;
                var height = window.innerHeight * 0.99;
                renderer = new THREE.WebGLRenderer({antialias: true});
                renderer.setSize(width, height);
                $container.append(renderer.domElement);

                scene = new THREE.Scene();

                camera = new THREE.PerspectiveCamera(
                    35,             // Field of view
                    width / height, // Aspect ratio
                    0.01,           // Near plane
                    10000           // Far plane
                );
                camera.position.set(500, 000, 2000);
                camera.lookAt(scene.position);

                scene.add(camera);

                var parent = new THREE.Object3D();
                parent.position.y = 50;
                scene.add( parent );

                paths.forEach(function(p){
                    var shape = transformSVGPath(p);
                    try {
                        var shapeMesh = createShape(shape, 0xFF0000, 0, 0, 0, Math.PI, 0, 0, 1);
                        parent.add(shapeMesh);
                    }
                    catch(q){}
                });

                /*for (var path in livecoding.json.paths) {
                    if (path != 'mi') {
                        var shape = transformSVGPath(livecoding.json.paths[path]);
                        var shapeMesh = createShape(shape, 0xFF0000, 0, 0, 0, Math.PI, 0, 0, 1);
                        parent.add(shapeMesh);
                    }
                }*/

                var light = new THREE.PointLight(parseInt('#FFFFFF'.replace('#', '0x')));
                light.position.set(10, 13, 7);
                scene.add(light);

                /*controls = new THREE.FlyControls(camera);
                controls.movementSpeed = 100;
                controls.rollSpeed = Math.PI / 12;;
                controls.autoForward = false;
                controls.dragToLook = false;*/

            };

            function animate() {

                requestAnimationFrame(animate);
                renderer.render(scene, camera);

                /*var delta = clock.getDelta();
                controls.update(delta);*/
            };

            function createShape( shape, color, x, y, z, rx, ry, rz, s ) {
                // flat shape

                var geometry = new THREE.ShapeGeometry( shape );
                var material = new THREE.MeshBasicMaterial({
                    color: color,
                    side: THREE.DoubleSide,
                    overdraw: true
                });

                var mesh = new THREE.Mesh( geometry, material );
                mesh.position.set( x, y, z );
                mesh.rotation.set( rx, ry, rz );
                mesh.scale.set( s, s, s );

                return mesh;
            }

            function transformSVGPath(pathStr) {

                const DIGIT_0 = 48, DIGIT_9 = 57, COMMA = 44, SPACE = 32, PERIOD = 46,
                    MINUS = 45;

                var path = new THREE.Shape();

                var idx = 1, len = pathStr.length, activeCmd,
                    x = 0, y = 0, nx = 0, ny = 0, firstX = null, firstY = null,
                    x1 = 0, x2 = 0, y1 = 0, y2 = 0,
                    rx = 0, ry = 0, xar = 0, laf = 0, sf = 0, cx, cy;

                function eatNum() {
                    var sidx, c, isFloat = false, s;
                    // eat delims
                    while (idx < len) {
                        c = pathStr.charCodeAt(idx);
                        if (c !== COMMA && c !== SPACE)
                            break;
                        idx++;
                    }
                    if (c === MINUS)
                        sidx = idx++;
                    else
                        sidx = idx;
                    // eat number
                    while (idx < len) {
                        c = pathStr.charCodeAt(idx);
                        if (DIGIT_0 <= c && c <= DIGIT_9) {
                            idx++;
                            continue;
                        }
                        else if (c === PERIOD) {
                            idx++;
                            isFloat = true;
                            continue;
                        }

                        s = pathStr.substring(sidx, idx);
                        return isFloat ? parseFloat(s) : parseInt(s);
                    }

                    s = pathStr.substring(sidx);
                    return isFloat ? parseFloat(s) : parseInt(s);
                }

                function nextIsNum() {
                    var c;
                    // do permanently eat any delims...
                    while (idx < len) {
                        c = pathStr.charCodeAt(idx);
                        if (c !== COMMA && c !== SPACE)
                            break;
                        idx++;
                    }
                    c = pathStr.charCodeAt(idx);
                    return (c === MINUS || (DIGIT_0 <= c && c <= DIGIT_9));
                }

                var canRepeat;
                activeCmd = pathStr[0];
                while (idx <= len) {
                    canRepeat = true;
                    switch (activeCmd) {
                        // moveto commands, become lineto's if repeated
                        case 'M':
                            x = eatNum();
                            y = eatNum();
                            path.moveTo(x, y);
                            activeCmd = 'L';
                            break;
                        case 'm':
                            x += eatNum();
                            y += eatNum();
                            path.moveTo(x, y);
                            activeCmd = 'l';
                            break;
                        case 'Z':
                        case 'z':
                            canRepeat = false;
                            if (x !== firstX || y !== firstY)
                                path.lineTo(firstX, firstY);
                            break;
                        // - lines!
                        case 'L':
                        case 'H':
                        case 'V':
                            nx = (activeCmd === 'V') ? x : eatNum();
                            ny = (activeCmd === 'H') ? y : eatNum();
                            path.lineTo(nx, ny);
                            x = nx;
                            y = ny;
                            break;
                        case 'l':
                        case 'h':
                        case 'v':
                            nx = (activeCmd === 'v') ? x : (x + eatNum());
                            ny = (activeCmd === 'h') ? y : (y + eatNum());
                            path.lineTo(nx, ny);
                            x = nx;
                            y = ny;
                            break;
                        // - cubic bezier
                        case 'C':
                            x1 = eatNum(); y1 = eatNum();
                        case 'S':
                            if (activeCmd === 'S') {
                                x1 = 2 * x - x2; y1 = 2 * y - y2;
                            }
                            x2 = eatNum();
                            y2 = eatNum();
                            nx = eatNum();
                            ny = eatNum();
                            path.bezierCurveTo(x1, y1, x2, y2, nx, ny);
                            x = nx; y = ny;
                            break;
                        case 'c':
                            x1 = x + eatNum();
                            y1 = y + eatNum();
                        case 's':
                            if (activeCmd === 's') {
                                x1 = 2 * x - x2;
                                y1 = 2 * y - y2;
                            }
                            x2 = x + eatNum();
                            y2 = y + eatNum();
                            nx = x + eatNum();
                            ny = y + eatNum();
                            path.bezierCurveTo(x1, y1, x2, y2, nx, ny);
                            x = nx; y = ny;
                            break;
                        // - quadratic bezier
                        case 'Q':
                            x1 = eatNum(); y1 = eatNum();
                        case 'T':
                            if (activeCmd === 'T') {
                                x1 = 2 * x - x1;
                                y1 = 2 * y - y1;
                            }
                            nx = eatNum();
                            ny = eatNum();
                            path.quadraticCurveTo(x1, y1, nx, ny);
                            x = nx;
                            y = ny;
                            break;
                        case 'q':
                            x1 = x + eatNum();
                            y1 = y + eatNum();
                        case 't':
                            if (activeCmd === 't') {
                                x1 = 2 * x - x1;
                                y1 = 2 * y - y1;
                            }
                            nx = x + eatNum();
                            ny = y + eatNum();
                            path.quadraticCurveTo(x1, y1, nx, ny);
                            x = nx; y = ny;
                            break;
                        // - elliptical arc
                        case 'A':
                            rx = eatNum();
                            ry = eatNum();
                            xar = eatNum() * DEGS_TO_RADS;
                            laf = eatNum();
                            sf = eatNum();
                            nx = eatNum();
                            ny = eatNum();
                            if (rx !== ry) {
                                console.warn("Forcing elliptical arc to be a circular one :(",
                                    rx, ry);
                            }
                            // SVG implementation notes does all the math for us! woo!
                            // http://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
                            // step1, using x1 as x1'
                            x1 = Math.cos(xar) * (x - nx) / 2 + Math.sin(xar) * (y - ny) / 2;
                            y1 = -Math.sin(xar) * (x - nx) / 2 + Math.cos(xar) * (y - ny) / 2;
                            // step 2, using x2 as cx'
                            var norm = Math.sqrt(
                                (rx*rx * ry*ry - rx*rx * y1*y1 - ry*ry * x1*x1) /
                                (rx*rx * y1*y1 + ry*ry * x1*x1));
                            if (laf === sf)
                                norm = -norm;
                            x2 = norm * rx * y1 / ry;
                            y2 = norm * -ry * x1 / rx;
                            // step 3
                            cx = Math.cos(xar) * x2 - Math.sin(xar) * y2 + (x + nx) / 2;
                            cy = Math.sin(xar) * x2 + Math.cos(xar) * y2 + (y + ny) / 2;

                            var u = new THREE.Vector2(1, 0),
                                v = new THREE.Vector2((x1 - x2) / rx,
                                    (y1 - y2) / ry);
                            var startAng = Math.acos(u.dot(v) / u.length() / v.length());
                            if (u.x * v.y - u.y * v.x < 0)
                                startAng = -startAng;

                            // we can reuse 'v' from start angle as our 'u' for delta angle
                            u.x = (-x1 - x2) / rx;
                            u.y = (-y1 - y2) / ry;

                            var deltaAng = Math.acos(v.dot(u) / v.length() / u.length());
                            // This normalization ends up making our curves fail to triangulate...
                            if (v.x * u.y - v.y * u.x < 0)
                                deltaAng = -deltaAng;
                            if (!sf && deltaAng > 0)
                                deltaAng -= Math.PI * 2;
                            if (sf && deltaAng < 0)
                                deltaAng += Math.PI * 2;

                            path.absarc(cx, cy, rx, startAng, startAng + deltaAng, sf);
                            x = nx;
                            y = ny;
                            break;
                        default:
                            throw new Error("weird path command: " + activeCmd);
                    }
                    if (firstX === null) {
                        firstX = x;
                        firstY = y;
                    }
                    // just reissue the command
                    if (canRepeat && nextIsNum())
                        continue;
                    activeCmd = pathStr[idx++];
                }

                return path;
            }
        }
    }

    HomeCtrl.$inject = ['$http'];

    angular.module(DOTF.modules.main)
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: 'views/home/index.html',
                    controller: HomeCtrl,
                    controllerAs: 'home'
                });
        }]);
}());