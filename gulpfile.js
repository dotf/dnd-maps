/**
 * Created by ppa on 01.12.2015.
 */
/*global require*/
(function(){
    'use strict';
    var gulp = require('gulp'),
        concat = require('gulp-concat'),
        less = require('gulp-less');

    gulp.task('directives', function () {
        gulp.src(['js/directives/**/*.js'])
            .pipe(concat('directives.js'))
            .pipe(gulp.dest('js'));
    });

    gulp.task('services', function () {
        gulp.src(['js/services/**/*.js'])
            .pipe(concat('services.js'))
            .pipe(gulp.dest('js'));
    });

    gulp.task('views', function () {
        gulp.src(['views/**/*.js'])
            .pipe(concat('views.js'))
            .pipe(gulp.dest('js'));
    });

    gulp.task('less', function () {
        gulp.src('css/app.less')
            .pipe(less())
            .pipe(gulp.dest('css'));
    });

    gulp.task('watch', function () {
        gulp.watch(['js/services/**/*.js'], ['services']);
        gulp.watch('views/**/*.js', ['views']);
        gulp.watch('js/directives/**/*.js', ['directives']);
        gulp.watch(['css/*.less'], ['less']);
    });


    gulp.task('fonts', function () {
        gulp.src('bower_components/font-awesome/fonts/**/*')
            .pipe(gulp.dest('fonts'));
    });

    gulp.task('dev', [
        'less',
        'views',
        'directives',
        'services',
        'watch',
        'fonts'
    ]);
    gulp.task('default', [
        'dev'
    ]);

}());