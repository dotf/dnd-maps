/**
 * Created by ppa on 01.12.2015.
 */
/* global angular, DOTF */
(function(){
    'use strict';

    angular.module(DOTF.modules.main)
        .service('OpenStreetMap', ['$resource', function($resource){
            return $resource('', {}, {
                getBoundsData: {method: 'GET', url: DOTF.apis.osm + '/map'}
            });
        }]);
}());