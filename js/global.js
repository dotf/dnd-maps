/**
 * Created by ppa on 01.12.2015.
 */
(function(){
    'use strict';
    window.DOTF = {
        modules: {
            main: 'dndmaps'
        },
        locations: {
            defaultLocation: {
                zoom: 7
            },
            currentLocation: {
                zoom: 17
            }
        },
        apis: {
            osm: 'http://www.openstreetmap.org/api/0.6',
            dotf: 'http://localhost:8090'
        }
    };
}());