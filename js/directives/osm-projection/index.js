/**
 * Created by ppa on 02.12.2015.
 */
/* global angular, DOTF, d3 */
(function(){
    'use strict';
    angular.module(DOTF.modules.main)
        .directive('osmProjection', [function(){
            return {
                restrict: 'EA',
                link: function(){
                    d3.json("us-states.json", function(error, collection) {
                        if (error) {
                            throw error;
                        }


                        console.log(collection);
                    });
                }
            };
        }]);
}());