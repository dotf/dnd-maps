/**
 * Created by ppa on 01.12.2015.
 */
/* global angular, DOTF, google, L */
(function(){
    'use strict';

    angular.module(DOTF.modules.main)
        .directive('dotfMap', [function(){
            return {
                restrict: 'EA',
                scope: {
                  map: '=dotfMap'
                },
                link: function(scope, elem){

                    if (navigator.geolocation){
                        navigator.geolocation.getCurrentPosition(renderCurrentLocation, renderDefaultLocation);
                    }

                    function renderCurrentLocation(position){
                        /*scope.map = new google.maps.Map(elem[0], {
                            center: {lat: position.coords.latitude, lng: position.coords.longitude},
                            zoom: 19
                        });*/

                        scope.map = L.map(elem[0], {
                            center: [position.coords.latitude, position.coords.longitude],
                            zoom: 19
                        });

                        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                            attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
                            maxZoom: 19
                        }).addTo(scope.map);

                       /* google.maps.event.addListener(scope.map, 'zoom_changed', function() {
                            console.log(scope.map.getZoom());
                        });*/

                        scope.$apply();
                    }

                    function renderDefaultLocation(){
                        /*scope.map = new google.maps.Map(elem[0], {
                            center: {lat: 0, lng: 0},
                            zoom: 2
                        });*/

                        scope.map = L.map(elem[0], {
                            center: [0, 0],
                            zoom: 2
                        });

                        /*google.maps.event.addListener(scope.map, 'zoom_changed', function() {
                            console.log(scope.map.getZoom());
                        });*/

                        scope.$apply();

                    }
                }
            };
        }]);
}());