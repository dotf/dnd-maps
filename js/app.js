/**
 * Created by ppa on 01.12.2015.
 */
/* global angular, DOTF */
(function(){
    'use strict';

    angular.module(DOTF.modules.main, ['ngResource', 'ui.router'])
        .config(['$urlRouterProvider', function ($urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
        }]);
}());