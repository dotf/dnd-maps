**Installation**


----------


 1. Install nodejs: https://nodejs.org/en/
 2. Install bower: run `npm install -g bower`
 3. Install node packages: run `npm install`
 4. Install bower packages: run `bower install`
 5. Run gulp task: `gulp dev`
 6. Start server: `node server.js`
 7. Open index.html in browser
 8. (optional) Install Webstorm https://www.jetbrains.com/webstorm/